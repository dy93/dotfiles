set nocompatible
set modeline
" vim settings
set encoding=utf-8
set fileencodings=utf-8,cp950
syntax on

set shiftwidth=4
set tabstop=4
set softtabstop=4
set ai
set ruler
set backspace=2
set ic " Ignore case in search patterns.
set ru " show cursor position
set hlsearch
set incsearch
set smartindent
set confirm
set history=100
set laststatus=2
set statusline=%4*%<\%m%<[%f\%r%h%w]\ [%{&ff},%{&fileencoding},%Y]%=\[Position=%l,%v,%p%%]
set tabpagemax=50
set nu

set foldmethod=syntax
"set nofoldenable
set foldlevel=10

" auto complete
autocmd FileType html setl omnifunc=htmlcomplete#CompleteTags noci
autocmd FileType css setl omnifunc=csscomplete#CompleteCSS noci
autocmd FileType xml setl omnifunc=xmlcomplete#CompleteTags
autocmd FileType php setl omnifunc=phpcomplete#CompletePHP

" show trailing white spaces
highlight WhitespaceEOL ctermbg=red guibg=red
match WhitespaceEOL /\s\+$/

" Automatically remove trailing whitespace
autocmd BufWritePre * :%s/\s\+$//e


" Tell vim to remember certain things when exit
"  '10  :  marks will be remembered for up to 10 previously edited files
"  "100 :  will save up to 100 lines for each register
"  :20  :  up to 20 lines of command-line history will be remembered
"  %    :  saves and restores the buffer list
"  n... :  where to save the viminfo files
set viminfo='10,\"100,:20,%,n~/.viminfo
" restore cursor
function! ResCur()
    if line("'\"") <= line("$")
        normal! g`"
        return 1
    endif
endfunction

augroup resCur
    autocmd!
    autocmd BufWinEnter * call ResCur()
augroup END



" ===== install plugins =====

call plug#begin('~/.vim/plugged')

" snippets
Plug 'SirVer/ultisnips' | Plug 'honza/vim-snippets'

" Nerdtree
Plug 'scrooloose/nerdtree', { 'on':  'NERDTreeToggle' }
" Nerdtree & git
Plug 'Xuyuanp/nerdtree-git-plugin'

" fuzzy finder
Plug 'junegunn/fzf', { 'dir': '~/.fzf', 'do': './install --all' }

" yapf formater
Plug 'dy93/vim-yapf', { 'for': ['python'] }

" gitgutter
Plug 'airblade/vim-gitgutter'

" color theme
Plug 'vim-scripts/darknight256.vim'

"
Plug 'shawncplus/phpcomplete.vim', { 'for': ['php'] }

" YouCompleteMe
Plug 'Valloric/YouCompleteMe', { 'for': ['c', 'cpp', 'python', 'javascript', 'php', 'lua'] }

" YCM-Generator
Plug 'rdnetto/YCM-Generator', { 'for': ['c', 'cpp'], 'branch': 'stable' }

" syntastic
Plug 'vim-syntastic/syntastic', { 'for': ['lua', 'vim', 'python'] }

" xolox/vim-lua-ftplugin
Plug 'xolox/vim-lua-ftplugin', { 'for': ['lua'] } | Plug 'xolox/vim-misc', { 'for': ['lua'] }

" hjson
Plug 'hjson/vim-hjson', {'for': ['json', 'hjson'] }

" tagbar
Plug 'majutsushi/tagbar', { 'on': 'TagbarToggle' }

" Ansible
Plug 'pearofducks/ansible-vim', { 'do': 'cd ./UltiSnips; python2 generate.py' }


call plug#end()

" ===== self defined functions =====
function! SwitchSourceHeader()
python3 << endpython
import vim
from os import path
import os
curr_file = vim.eval('expand("%:t")')
curr_ext = path.splitext(curr_file)[1]
curr_file = path.splitext(curr_file)[0]
SOURCE_EXTENSIONS = [ '.cpp', '.cxx', '.cc', '.c', '.m', '.mm' ]
HEADER_EXTEMSIONS = [ '.h', '.hxx', '.hpp', '.hh' ]
if curr_ext in SOURCE_EXTENSIONS:
  for dirpath, dirnames, filenames in os.walk('.'):
    for f in filenames:
      for ext in HEADER_EXTEMSIONS:
        if f == curr_file + ext:
          new_path = path.join(dirpath, f)
          vim.command('find! {0}'.format(new_path))
elif curr_ext in HEADER_EXTEMSIONS:
  for dirpath, dirnames, filenames in os.walk('.'):
    for f in filenames:
      for ext in SOURCE_EXTENSIONS:
        if f == curr_file + ext:
          new_path = path.join(dirpath, f)
          vim.command('find! {0}'.format(new_path))
endpython
endfunction

function! MyAutoFormat()
  let g:gitgutter_async = 0
  execute "GitGutterAll"
  let hunks = GitGutterGetHunks()
  if len(hunks) > 0
    let lines = []
    for hunk in GitGutterGetHunks()
      if hunk[2] <= hunk[2]+hunk[3]-1
        "execute join([hunk[2], hunk[2]+hunk[3]-1], ",")."Autoformat"
		call add(lines, '-l '.hunk[2].'-'.(hunk[2]+hunk[3]-1))
      else
        "execute join([hunk[2]+hunk[3]-1, hunk[2]], ",")."Autoformat"
		call add(lines, '-l '.(hunk[2]+hunk[3]-1).'-'.hunk[2])
      endif
    endfor
	echom "Yapf --style ~/.style.yapf ".join(lines, ' ')
	let save = winsaveview()
	execute "Yapf --style ~/.style.yapf ".join(lines, ' ')
	call winrestview(save)
  else
	"execute "Autoformat"
	execute "Yapf --style ~/.style.yapf"
  endif
  let g:gitgutter_async = 1
endfunction

function! MyNERDTreeFind()
  if !exists('t:NERDTreeBufName') || !bufwinnr(t:NERDTreeBufName) != -1
    execute "NERDTreeToggle"
  endif
  execute "NERDTreeFind"
endfunction

" ===== plugin settings ======
" snippets
" Trigger configuration. Do not use <tab> if you use https://github.com/Valloric/YouCompleteMe.
let g:UltiSnipsExpandTrigger="<c-f>"
let g:UltiSnipsJumpForwardTrigger="<c-b>"
let g:UltiSnipsJumpBackwardTrigger="<c-z>"
let g:ultisnips_python_style="sphinx"
" If you want :UltiSnipsEdit to split your window.
let g:UltiSnipsEditSplit="vertical"

" tagbar
let g:tagbar_width = 30
let g:tagbar_left = 1
set tags=./tags;/
set t_Co=256

" nerdtree
" open nerdtree when no file specified
autocmd StdinReadPre * let s:std_in=1
"autocmd VimEnter * if argc() == 0 && !exists("s:std_in") | NERDTree | endif
let NERDTreeIgnore=['\.o$', '__pycache__', '\.pyc$']

" formater
""let g:formatdef_my_yapf = "'yapf --style ~/.style.yapf -l '.a:firstline.'-'.a:lastline"
""let g:formatters_python = ['my_yapf']
""let g:autoformat_verbosemode=1
"let g:autoformat_autoindent = 0
"let g:autoformat_retab = 0
"let g:autoformat_remove_trailing_spaces = 0
autocmd BufWritePre *.py :call MyAutoFormat()

" GitGutter
highlight GitGutterAdd ctermbg=green
highlight GitGutterChange ctermbg=yellow
highlight GitGutterDelete ctermbg=red
highlight GitGutterChangeDelete ctermbg=red

" color scheme
colorscheme darknight256

" YouCompleteMe
let g:ycm_filetype_blacklist = {
      \ 'tagbar' : 1,
      \ 'qf' : 1,
      \ 'notes' : 1,
      \ 'markdown' : 1,
      \ 'unite' : 1,
      \ 'text' : 1,
      \ 'vimwiki' : 1,
      \ 'log': 1
      \}
let g:ycm_confirm_extra_conf = 0
let g:ycm_autoclose_preview_window_after_completion = 0
let g:ycm_autoclose_preview_window_after_insertion = 1
let g:ycm_global_ycm_extra_conf = '~/.vim/.ycm_extra_conf.py'
let g:ycm_complete_in_comments = 1
let g:ycm_collect_identifiers_from_comments_and_strings = 1
let g:ycm_collect_identifiers_from_tags_files = 1
let g:ycm_python_binary_path = 'python'
let g:ycm_semantic_triggers =  {
  \   'c' : ['->', '.'],
  \   'objc' : ['->', '.', 're!\[[_a-zA-Z]+\w*\s', 're!^\s*[^\W\d]\w*\s',
  \             're!\[.*\]\s'],
  \   'ocaml' : ['.', '#'],
  \   'cpp,objcpp' : ['->', '.', '::'],
  \   'perl' : ['->'],
  \   'php' : ['->', '::'],
  \   'cs,java,javascript,typescript,d,python,perl6,scala,vb,elixir,go' : ['.'],
  \   'ruby' : ['.', '::'],
  \   'lua' : ['.', ':'],
  \   'erlang' : [':'],
  \ }

" syntastic
let g:syntastic_lua_checkers = ['luacheck']
let g:syntastic_python_checkers = ['flake8']
let g:syntastic_enable_vim_checker = 1

" xolox/vim-lua-ftplugin
let g:lua_check_syntax = 0 " just use syntastic to check lua
let g:lua_complete_omni = 1
let g:lua_safe_omni_modules = 1

" indentline
let g:indentLine_color_term = 239
let g:indentLine_bgcolor_term = 0
" let g:indentLine_char = '│'

" ===== self defined hot keys =====
map      <F9>  <ESC>:NERDTreeToggle<CR>
nmap     <F8>  :TagbarToggle<CR>
nnoremap <C-l> :tabclose<CR>
map      <C-n> <ESC>:tabnew<CR>
nnoremap +     <ESC>:vertical resize +1<CR>
nnoremap -     <ESC>:vertical resize -1<CR>
nnoremap 1     :tabprevious<CR>
nnoremap 2     :tabnext<CR>
map      <F10> <ESC>:GitGutterToggle<CR>
map      <F12> <ESC>:YcmCompleter GoTo<CR>
map      <F11> <ESC>:call SwitchSourceHeader()<CR>
nnoremap 3     :YcmCompleter GetDoc<CR>
nnoremap 4     <C-w><C-z>
nnoremap <C-p> :FZF<CR>
nnoremap <C-b> :call MyNERDTreeFind()<CR>

