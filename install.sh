#!/usr/bin/env bash
MY_DIR=`dirname $0`
cd $MY_DIR
MY_DIR=`pwd`
cd - > /dev/null

#
# bashrc
#
ln -s -f $MY_DIR/.git-prompt.sh ~/ # git branch info
ln -s -f $MY_DIR/.bashrc ~/

#
# vim settings
#
rm -rf ~/.vim
ln -s -f $MY_DIR/.vimrc ~/
curl -fLo ~/.vim/autoload/plug.vim --create-dirs \
	https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
vim +PlugInstall +qall
ln -s -f $MY_DIR/.tern-config ~/

#
# git config
#

# backup git user settings
git_user=`git config --global user.name`
git_email=`git config --global user.email`

cp $MY_DIR/.gitconfig ~/

# restore git user settings
git config --global user.name $git_user
git config --global user.email $git_email

#
# screen
#
ln -s -f $MY_DIR/.screenrc ~/
ln -s -f $MY_DIR/.screenrc-inner ~/

#
# tig
#
ln -s -f $MY_DIR/.tigrc ~/
